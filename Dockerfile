FROM fodel/php-imagick

MAINTAINER WajidAbid <wajid@fo-del.com>

COPY app/ /var/www/html/

EXPOSE 80